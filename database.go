package main

import (
	"fmt"
	"gilab.com/programaticreviews/golang-gin-poc/entity"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

var err error

func InitialMigration() {
	dsn := "root:root@tcp(127.0.0.1:3306)/golang_demo?charset=utf8mb4&parseTime=True&loc=Local"
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println(err.Error())
		panic("Can't connect to DB!")
	}

	DB.AutoMigrate(&entity.Video{}, entity.Author{})
}
