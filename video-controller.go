package main

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"gilab.com/programaticreviews/golang-gin-poc/cache"
	"gilab.com/programaticreviews/golang-gin-poc/encryptionDecryption"
	"gilab.com/programaticreviews/golang-gin-poc/entity"
	"gilab.com/programaticreviews/golang-gin-poc/service"
	"github.com/gin-gonic/gin"
	"log"
	"time"
)

var redisVideo cache.VideoCache = cache.NewRedis("localhost:6379", 0, 0)

type VideoController interface {
	FindByTitle(title string) (entity.Video, error)
	FindAll() ([]entity.Video, error)
	Save(ctx *gin.Context) error
	DeleteByTitle(title string) error
	UpdateByTitle(ctx *gin.Context) (entity.Video, error)
	EncrytionDecryption(ctx *gin.Context) error
}

type controller struct {
	service service.VideoService
}

func New(service service.VideoService) VideoController {
	return &controller{
		service: service,
	}
}

func (c *controller) FindByTitle(title string) (entity.Video, error) {
	videosLocal, isFound := videoLocalCache.Get("videos")
	var videoForReturn entity.Video
	if isFound {
		isVideoInLocalCache := false
		fmt.Println("There are videos in Local cache, now all it needs to find is the '" + title + "' video")
		videos := videosLocal.([]entity.Video)
		for i := 0; i < len(videos); i++ {
			video := videos[i]
			if video.Title == title {
				isVideoInLocalCache = true
				fmt.Println("The video is in Local cache. So no need to search it in redis neither Db.")
				return video, nil
			}
		}
		if !isVideoInLocalCache {
			fmt.Println("The video is not in the Local cache. Now search it in redis.")
			video, err := redisVideo.GetVideo("Video:" + title)
			if err != nil {
				return *video, err
			}
			if video == nil {
				var videoFromDB entity.Video
				fmt.Println("The video is not in the Redis. Now search it in DB.")
				//videoFromDB, errVideoFromDB := dBFindByTitle(title);
				err := DB.Preload("Author").Where("title = ?", title).First(&videoFromDB).Error
				if err != nil {
					return videoFromDB, err
				}
				redisVideo.SetVideo("Video:" + title, &videoFromDB)
				return videoFromDB, nil
			}
			return *video, nil
		}
	} else {
		fmt.Println("There is no videos in the Local cache. Now search it in redis.")
		video, err := redisVideo.GetVideo("Video:" + title)
		if err != nil {
			return *video, err
		}
		if video == nil {
			var videoFromDB entity.Video
			fmt.Println("The video is not in the Redis. Now search it in DB.")
			err := DB.Preload("Author").Where("title = ?", title).First(&videoFromDB).Error
			if err != nil {
				return videoFromDB, err
			}
			redisVideo.SetVideo("Video:" + title, &videoFromDB)
			return videoFromDB, nil
		} else {
			return *video, nil
		}
	}
	return videoForReturn, err
}

func dBFindByTitle(title string) (entity.Video, error) {
	var video entity.Video
	var author entity.Author
	var authors []entity.Author
	//normal query
	//DB.Where("title = ?", title).First(&video)
	//join query
	//DB.Raw("SELECT * FROM videos v LEFT JOIN authors a ON v.id = a.video_id where v.title = ?", title).Scan(&video).Scan(&authors)
	//rows, err := DB.Raw("SELECT v.title, v.description, v.url, a.firstname, a.lastname, a.age, a.email FROM videos v JOIN authors a ON v.id = a.video_id where v.title = ?", title).Rows()
	rows, err := DB.Raw("SELECT v.id, v.created_at, v.description, v.url, a.id, a.created_at, a.firstname, "+
		"a.lastname, a.age, a.email FROM videos v JOIN authors a ON v.id = a.video_id where v.title = ?", title).Rows()

	defer rows.Close()

	if err != nil {
		return video, err
	}
	for rows.Next() {
		//description, url, firstName, lastName, age, email := rows.Scan(&video.Description, &video.URL, &author.Firstname, &author.Lastname, &author.Age, &author.Email)
		rows.Scan(&video.ID, &video.CreatedAt, &video.Description, &video.URL, &author.ID, &author.CreatedAt, &author.Firstname, &author.Lastname, &author.Age, &author.Email)
		video.Title = title

		authors = append(authors, author)
		//DB.ScanRows(rows, &video)
		// do something
	}
	video.Author = authors
	return video, err
}

func (c *controller) UpdateByTitle(ctx *gin.Context) (entity.Video, error) {
	title := ctx.Param("title")
	updatedDesc := ctx.Request.PostFormValue("description")
	updatedURL := ctx.Request.PostFormValue("url")
	var video entity.Video
	DB.Where("title = ?", title).First(&video)
	video.Description = updatedDesc
	video.URL = updatedURL
	err := DB.Save(&video).Error
	if err != nil {
		return video, err
	}
	return video, nil
}

func (c *controller) DeleteByTitle(title string) error {
	var video entity.Video
	err := DB.Where("title = ?", title).Delete(&video).Error
	return err
}

func (c *controller) FindAll() ([]entity.Video, error) {
	videosLocal, isFound := videoLocalCache.Get("videos")
	if isFound {
		fmt.Println("From go cache")
		videos := videosLocal.([]entity.Video)
		return videos, nil
	} else {
		var videos []entity.Video
		fmt.Println("From DB")
		err := DB.Preload("Author").Find(&videos).Error
		if err != nil {
			return videos, err
		}
		videoLocalCache.Set("videos", videos, 5*time.Minute)
		return videos, nil
	}
}

func (c *controller) Save(ctx *gin.Context) error {
	var video entity.Video
	title := ctx.PostForm("title")
	description := ctx.PostForm("description")
	url := ctx.PostForm("url")
	author := ctx.PostForm("author")
	var authors []entity.Author
	json.Unmarshal([]byte(author), &authors)

	if err != nil {
		return err
	}
	fmt.Println("Title: " + title + ", Description: " + description + ", URL: " + url + ", Author: " + author)
	video.Title = title
	video.Description = description
	video.URL = url
	video.Author = authors

	DB.Create(&video)
	videosLocal, isFound := videoLocalCache.Get("videos")
	if isFound {
		videos := videosLocal.([]entity.Video)
		allVideos := append(videos, video)
		videoLocalCache.Set("videos", allVideos, 5*time.Minute)
	} else {
		var videos []entity.Video
		allVideos := append(videos, video)
		videoLocalCache.Set("videos", allVideos, 5*time.Minute)
	}
	return nil
}

func (c *controller) EncrytionDecryption(ctx *gin.Context) error {
	data := ctx.PostForm("data")

	plaintext := []byte(data)
	fmt.Println("Original Data: ", data)
	ciphertext, err := encryptionDecryption.Encrypt(plaintext)
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Printf("%0x\n", ciphertext)
	encryptedString := hex.EncodeToString(ciphertext)
	fmt.Println("Encrypted Data: ", encryptedString)

	result, err := encryptionDecryption.Decrypt(ciphertext)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Decrypt Data: %s\n", result)

	return nil
}

func VideoToByteArrayConverter(video entity.Video) []byte {
	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(video)

	b := reqBodyBytes.Bytes()
	return b
}
