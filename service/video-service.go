package service

import (
	"gilab.com/programaticreviews/golang-gin-poc/entity"
	"github.com/jinzhu/gorm"
)

var(
	DB *gorm.DB
)

type VideoService interface {
	FindByTitle(title string) entity.Video
	Save(entity.Video) entity.Video
	FindAll() []entity.Video
}

type videoService struct {
	videos []entity.Video
}

func New() VideoService {
	return &videoService{}
}

func (service *videoService) Save(video entity.Video) entity.Video {
	service.videos = append(service.videos, video)
	//DB.Create(&video)
	return video
}

func (service *videoService) FindAll() []entity.Video {
	return service.videos
}

func (service *videoService) FindByTitle(title string) entity.Video {
	allVideos := service.videos
	var video entity.Video
	for i := 0; i < len(allVideos); i++ {
		if allVideos[i].Title == title {
			video = allVideos[i]
		}
	}
	return video
}
