package cache

import (
	"context"
	"encoding/json"
	"gilab.com/programaticreviews/golang-gin-poc/entity"
	"github.com/go-redis/redis/v8"
	"time"
)

type RedisCache struct {
	host    string
	db      int
	expires time.Duration
}

var ctx = context.Background()

func NewRedis(host string, db int, expires time.Duration) VideoCache {
	return &RedisCache{
		host:    host,
		db:      db,
		expires: expires,
	}
}

func (cache *RedisCache) getRedisClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     cache.host,
		Password: "",
		DB:       cache.db,
	})
}

func (cache *RedisCache) SetVideo(key string, video *entity.Video) error {
	redisClient := cache.getRedisClient()

	videoJson, err := json.Marshal(video)
	if err != nil {
		panic(err)
	}
	videoSetError := redisClient.Set(ctx, key, videoJson, cache.expires*time.Second).Err()
	if videoSetError != nil {
		return videoSetError
	}
	return nil
}
func (cache *RedisCache) GetVideo(key string) (*entity.Video, error) {
	redisClient := cache.getRedisClient()

	videoString, err := redisClient.Get(ctx, key).Result()
	if err != nil {
		panic(err)
		return nil, err
	}
	video := entity.Video{}
	if (videoString == ""){
		return nil, nil
	}
	err = json.Unmarshal([]byte(videoString), &video)
	if err != nil {
		return nil, err
	}
	return &video, nil
}

//
//import (
//	"fmt"
//	"github.com/go-redis/redis/v8"
//	"log"
//	"time"
//)
//
//func GetTerms(db gorm.DB, Terms []models.Terms) (err error) {
//
//
//	client := redis.NewClient(&redis.Options{
//		Addr: "localhost:6379",
//		Password: "",
//		DB: 0,
//	})
//
//	val,err:=client.Get("terms").Result()
//	if err !=nil{
//		fmt.Println("rediss er: ",err)
//	}
//	//cache exist
//	if len(val) !=0{
//		err := json2.Unmarshal([]byte(val), &Terms)
//		if err != nil {
//			log.Fatal(err)
//		}
//		val,err:=client.Get("terms").Result()
//		fmt.Println("get from redis")
//		if err !=nil{
//			fmt.Println(val)
//		}
//
//		//	fmt.Println("redis data: ",val)
//
//		//return
//	} else {
//		//	err = db.Raw("SELECT id, terms_white, terms_black FROM terms ").Scan(&Terms).Error
//
//		err = db.Raw("SELECT id, terms_white, terms_black FROM terms WHERE id = ?", 1).Scan(&Terms).Error
//		fmt.Println("get from db")
//		if err != nil {
//			log.Fatal(err)
//		}
//
//		json,err :=json2.Marshal(Terms)
//		if err !=nil{
//			fmt.Println(err)
//		}
//
//		err=client.Set("terms",json, 1*time.Minute).Err()
//		fmt.Println("set redis")
//		if err !=nil{
//			fmt.Println(err)
//		}
//	}
//
//	return nil
//}
