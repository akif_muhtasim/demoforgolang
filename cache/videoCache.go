package cache

import "gilab.com/programaticreviews/golang-gin-poc/entity"

type VideoCache interface {
	SetVideo(key string, video *entity.Video) error
	GetVideo(key string) (*entity.Video, error)
}
