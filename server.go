package main

import (
	"fmt"
	"gilab.com/programaticreviews/golang-gin-poc/middlewares"
	"gilab.com/programaticreviews/golang-gin-poc/service"
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/patrickmn/go-cache"
	"io"
	"net/http"
	"os"
	"time"
)

var (
	videoService    service.VideoService = service.New()
	videoController VideoController      = New(videoService)
)

var videoLocalCache = cache.New(5*time.Minute, 10*time.Minute)

// function for logger file
func setupLogOutput() {
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	setupLogOutput()

	server := gin.New()

	//server := gin.Default()
	//server.Use(gin.Recovery(), middlewares.Logger(), middlewares.BasicAuth(), gindump.Dump())

	server.Use(gin.Recovery(), middlewares.Logger(), middlewares.BasicAuth())

	//Database
	InitialMigration()
	//db, dbErr := gorm.Open("mysql", "root:root@tcp(127.0.0.1:3306)/golang_demo")
	//
	//if dbErr != nil {
	//	panic(dbErr.Error())
	//}
	//
	//defer db.Close()

	//dsn := "root:root@tcp(127.0.0.1:3306)/golang_demo?charset=utf8mb4&parseTime=True&loc=Local"
	//DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	//if err != nil {
	//	fmt.Println(err.Error())
	//	panic("Can't connect to DB!")
	//}
	//
	//DB.AutoMigrate(&entity.Video{})
	//Database end

	server.GET("/videos/:title", func(ctx *gin.Context) {
		//var w http.ResponseWriter
		//r *http.Request
		title := ctx.Param("title")
		video, err := videoController.FindByTitle(title)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"message": "Video is not in the DB",
			})
		} else {
			//ctx.JSON(200, video)
			//w.Header().Set("Cache-Control", "max-age=2592000")
			ctx.Writer.Header().Set("Cache-Control", "no-transform, public, max-age=2592000")
			ctx.JSON(200, video)
		}
	})

	server.DELETE("/videos/:title", func(ctx *gin.Context) {
		title := ctx.Param("title")
		err := videoController.DeleteByTitle(title)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
		} else {
			ctx.JSON(http.StatusOK, gin.H{
				"message": "Video that contains title: " + title + "has been deleted",
			})
		}
	})

	server.GET("/videos", func(ctx *gin.Context) {
		result, err := videoController.FindAll()
		//ctx.JSON(200, videoController.FindAll())
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			ctx.JSON(http.StatusOK, result)
		}
	})

	server.POST("/plainText", func(ctx *gin.Context) {
		headerKey := ctx.Request.Header.Get("headerKey")
		is_must := ctx.Request.Header.Get("is_must")
		fmt.Println(headerKey + " " + is_must)

		err := videoController.Save(ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			ctx.JSON(http.StatusOK, gin.H{
				"message": "Video Input is Valid!!!",
			})
		}
	})

	server.PATCH("/videos/:title", func(ctx *gin.Context) {
		headerKey := ctx.Request.Header.Get("headerKey")
		is_must := ctx.Request.Header.Get("is_must")
		fmt.Println(headerKey + " " + is_must)

		video, err := videoController.UpdateByTitle(ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			ctx.JSON(http.StatusOK, video)
		}
	})

	server.POST("/encryptionDecryption", func(ctx *gin.Context) {
		headerKey := ctx.Request.Header.Get("headerKey")
		is_must := ctx.Request.Header.Get("is_must")
		fmt.Println(headerKey + " " + is_must)

		err := videoController.EncrytionDecryption(ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			ctx.JSON(http.StatusOK, gin.H{
				"message": "Encryption Decryption Displaying in the console!!!",
			})
		}
	})
	server.Run(":8080")
}
