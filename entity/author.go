package entity

import "github.com/jinzhu/gorm"

type Author struct {
	gorm.Model
	Firstname string `json:"firstname" binding:"required"`
	Lastname  string `json:"lastname" binding:"required"`
	Age       int8   `gorm:"-" json:"age" binding:"gte=1,lte=60"`
	Email     string `json:"email" binding:"required,email"`
	VideoId   uint
}
