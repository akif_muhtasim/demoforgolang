package entity

import "github.com/jinzhu/gorm"

type Video struct {
	gorm.Model
	Title       string `gorm:"type:varchar(100)" json:"title" binding:"min=2,max=10,required"`
	Description string `gorm:"type:varchar(500)" json:"description" binding:"max=20"`
	URL         string `gorm:"type:varchar(100)" json:"url" binding:"required,url"`
	//Author      Person `json:"author" binding:"required"` `gorm:"foreignkey:Email;references:title"`
	Author 		[]Author
}
