package middlewares

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"time"
)

func Logger() gin.HandlerFunc {
	return gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string{
		return fmt.Sprintf("This is a custom logger. ClientIp -> %s, - Time -> [%s], Method -> %s, Path -> %s, Statuscode -> %d, Latency -> %s \n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC822),
			param.Method,
			param.Path,
			param.StatusCode,
			param.Latency,
		)
	})

}
